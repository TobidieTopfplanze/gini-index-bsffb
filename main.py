# Import pandas
import pandas as pd

excelData = pd.read_excel("assets/skifahren.xlsx")
inputData = pd.DataFrame(excelData, columns=['Sonnig', 'Wochenende', 'Entfernung'])
outputData = excelData["Skifahren hat sich gelohnt"].tolist()
class Gi:
    def __init__(self, labels, labelMask,fn = lambda label : label):
        self.labels = labels
        self.labelMask = labelMask
        self.filterFn = fn
        self.update()

    def update(self):
        self.lableTable = dict.fromkeys(self.labelMask, 0) 
        giTemp = []

        for labelID in range(0, len(self.labels)):
            label = self.labels[labelID]
            data = self.filterFn(label)
            if data == self.labelMask[labelID]:
                self.lableTable[data] += 1

        for value in self.lableTable.values(): 
            giTemp.append(1 - (pow(value / len(self.labels), 2) + pow((len(self.labels) - sum(self.lableTable.values())) / len(self.labels), 2)))
            
        self.values = tuple(giTemp)

    @staticmethod
    def calculateImpurity(rawTestData, resultData, filter = lambda element: element): 
        testData = list(map(filter, rawTestData))
        giInput = dict([(_label, []) for _label in testData])
        for index in range(0, len(testData)):
            giInput[testData[index]].append("Yes" if testData[index] == resultData[index] else "No")

        return tuple(map(lambda data : Gi(data), giInput.values()))

class SplitGi:
    def __init__(self, giObjects: list[Gi]):
        self.giObjects = giObjects
        self.update()
        
    def getTotalLen(self):
        totalLen = 0
        for gi in self.giObjects:
            gi.update()
            totalLen += len(gi.labels)
        return totalLen;
        
    def update(self):
        self.value = 0
        totalLen = self.getTotalLen()
        for gi in self.giObjects:
            self.value += len(gi.labels) / totalLen * gi.value

giSonne = Gi(inputData['Sonnig'].tolist(), outputData)
giWochenende = Gi(inputData['Wochenende'].tolist(), outputData)
giEntfernung = Gi(inputData['Entfernung'].tolist(), outputData, lambda label : "Ja" if label >= 100 else "Nein")

print("Sonnig GI: {}".format(giSonne.values))
print("Wochenende GI: {}".format(giWochenende.values))
print("Entfernung GI: {}".format(giEntfernung.values))
# print("G.I Sonnig, Wochenende, Entfernung: {}".format(SplitGi([giSonne, giWochenende, giEntfernung]).value))
